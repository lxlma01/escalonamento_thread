

#define ROWS    10		//numero linhas
#define COLS    4		//numero colunas
#define Id      0		//Identificador do processo
#define Td      1		//Tempo duração do processo
#define Tc      2		//Tempo chegada do processo
#define Pr      3		//Prioridade do Processo
#define Tq      3       //Tempo de quantum



void *priop_runner(void *id)
{
    printf("\n Thread PRIOp Inicializada");

 int matrix[ROWS][COLS] = {{0}};   
	FILE *fp = fopen("entrada.txt", "r");
	
	fscanf(fp, "%*[^\n]");  // Read and discard a line
	for (int index = 0; index < ROWS; index++)
		fscanf(fp, "%d\t%d\t%d\t%d", &matrix[index][Id], &matrix[index][Td], &matrix[index][Tc], &matrix[index][Pr]);
	fclose (fp);

	for (int index = 0; index < ROWS; index++)
	{
		for (int row = index+1; row < ROWS; row++)
		{   
			if (matrix[row][Tc] < matrix[index][Tc])
			{
				int swap[4];
				for(int aux = 0; aux < 4; aux++ )
				{
					swap[aux]      		= matrix[index][aux];
					matrix[index][aux]  = matrix[row][aux];
					matrix[row][aux]	= swap[aux];
				}
			}
		
		}
	}

    int aux[ROWS];
	int waiting_time[ROWS+1];
	int life_time[ROWS+1];
	int total_waiting_time = 0;
	int total_life_time = 0;
    int count;
    int timer;
    int low;
	float average_waiting_time = 0;
	float average_life_time = 0;
	waiting_time[0] = 0;

    for (int index = 0; index < ROWS; index++)
    {
        aux[index] = matrix[index][Td];
    }
    matrix[9][Pr] = 10000;

    for (timer = 0; count != ROWS; timer ++)
    {
        // low = lowest_priority_process;
        low = 9;
        for(int index = 0; index < ROWS; index++)
        {

            if((matrix[low][Pr] > matrix[index][Pr]) && (matrix[index][Tc] <= timer) && (matrix[index][Td] > 0))
            {
                    low = index; 

            }

        }

        matrix[low][Td] = matrix[low][Td] - 1;




        if( matrix[low][Td] == 0 )
        {   
			count ++;
            waiting_time[low] = timer + 1 - matrix[low][Tc] - aux[low];
            life_time[low] = timer + 1 - matrix[low][Tc];

            total_life_time += life_time[low];
            total_waiting_time += waiting_time[low];
        }

    }

		printf("\n\nPi\tTempo de Execução\t Ciclo de Vida  Priop\n");

		for(int index = 0; index < ROWS; index++)
		{
			printf("%d\t\t%d\t\t\t%d\n", index + 1, waiting_time[index], life_time[index]);
		}


    average_life_time = (float)total_life_time/ROWS;
    average_waiting_time = (float)total_waiting_time/ROWS;

    printf("\n\n\nTEMPO DE ESPERA MÉDIO PRIOp = %.2f \n", average_waiting_time);
    printf("CICLO DE VIDA MÉDIO PRIOp = %.2f \n", average_life_time);
    printf("\nThread PRIOp Finalizada\n");

    pthread_exit (NULL) ;
}