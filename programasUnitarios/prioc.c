#include <stdio.h>

#define ROWS 10		//numero linhas
#define COLS 4		//numero colunas
#define Id   0		//Identificador do processo
#define Td   1		//Tempo duração do processo
#define Tc   2		//Tempo chegada do processo
#define Pr   3		//Prioridade do Processo



int get_matrix (int (*m)[COLS], FILE *fp)
{
	fscanf(fp, "%*[^\n]");
	for (int row = 0; row < ROWS; row++) 
	{
		for (int col = 0; col < COLS; col++) 
		{
			if (fscanf (fp, "%d", &m[row][col]) != 1) 
			{
				fprintf (stderr, "error reading m[%d][%d]\n", row, col);
			return 0;
			}
		}
	}
  
	return 1;
}

void print_matrix (int (*m)[COLS])
{
	for (int row = 0; row < ROWS; row++)
	{
		for (int col = 0; col < COLS; col++)
		{
			printf (col ? " %4d" : "%4d", m[row][col]);

		}

		putchar ('\n');

	}
}

void prioc_runner (int (*matrix)[COLS])
{
    // int aux[ROWS];
	int waiting_time[ROWS+1];
	int life_time[ROWS+1];
	int total_waiting_time = 0;
	int total_life_time = 0;
    int check_arrival_time;
    int completion_time =0;
	float average_waiting_time =0;
	float average_life_time = 0;

    check_arrival_time = matrix[0][Tc];
    for (int index; index < ROWS; index++)
        if(check_arrival_time != matrix[index][Tc])
            check_arrival_time = 1;


    if(check_arrival_time != 0)
    {
        waiting_time[0] = matrix[0][Tc];
        total_waiting_time += waiting_time[0]; 
        life_time[0] = matrix[0][Td] - matrix[0][Tc];
        completion_time = life_time[0];
        total_life_time += life_time[0];

        for (int index = 1; index < ROWS; index ++)
        {
            int priority = matrix[index][Pr];
            for (int row = index + 1; index < ROWS; index++)
            {
                if(priority > matrix[row][Pr] && matrix[row][Tc] <= completion_time)
                {
                    priority = matrix[row][Pr];
                 	int swap[4];
				    for(int aux = 0; aux < 4; aux++ )
                    {
                        swap[aux]      		= matrix[row][aux];
                        matrix[row][aux]    = matrix[index][aux];
                        matrix[index][aux]	= swap[aux];
                    }

                }
            }
            waiting_time[index] = completion_time - matrix[index][Tc];
            total_waiting_time += waiting_time[index];
            completion_time += matrix[index][Td];

            life_time[index] = completion_time - matrix[index][Tc];
            total_life_time += life_time[index];
        }

    }
    else
    {
        for(int index = 0; index < ROWS; index++)
        {
            int priority = matrix[index][Pr];
            for (int row = index + 1; row < ROWS; row ++)
            {
                if (priority > matrix[row][Pr] && matrix[row][Tc] <= completion_time)
                {
                    priority = matrix[row][Pr];
                    int swap[4];
				    for(int aux = 0; aux < 4; aux++ )
                    {
                        swap[aux]      		= matrix[row][aux];
                        matrix[row][aux]  = matrix[index][aux];
                        matrix[index][aux]	= swap[aux];
                    }
                }
            }
            waiting_time[index] = completion_time - matrix[index][Tc];
            total_waiting_time += waiting_time[index];
            completion_time += matrix[index][Td];

            life_time[index] = completion_time - matrix[index][Tc];
            total_life_time += life_time[index];
        }
    }

    average_waiting_time = (float)total_waiting_time/ROWS;
	average_life_time = (float)total_life_time/ROWS;

    printf("\n\n\nTEMPO DE ESPERA MÉDIO = %.2f \n", average_waiting_time);
	printf("CICLO DE VIDA MÉDIO = %.2f \n", average_life_time);
}



void sort_arrival_time (int (*matrix)[COLS])
{
    

	for (int index = 0; index < ROWS; index++)
	{
		for (int row = index+1; row < ROWS; row++)
		{   
			if (matrix[row][Tc] < matrix[index][Tc])
			{
				int swap[4];
				for(int aux = 0; aux < 4; aux++ )
				{
					swap[aux]      		= matrix[index][aux];
					matrix[index][aux]  = matrix[row][aux];
					matrix[row][aux]	= swap[aux];
				}
			}
		
		}
	}

	//print_matrix(matrix);
	prioc_runner(matrix);

}



int main (int argc, char **argv) {
  
	int matrix[ROWS][COLS] = {{0}};   
	/* use filename provided as 1st argument (stdin by default) */
	FILE *fp = argc > 1 ? fopen (argv[1], "r") : stdin;
	
	if (fp == NULL) {  /* validate file open for reading in caller */
	perror ("file open failed");
	return 1;
  }
  
	if (!get_matrix(matrix, fp))    
	{
		return 1;
	}
	fclose (fp);
	sort_arrival_time(matrix);

}