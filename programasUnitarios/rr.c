#include <stdio.h>

#define ROWS    10		//numero linhas
#define COLS    4		//numero colunas
#define Id      0		//Identificador do processo
#define Td      1		//Tempo duração do processo
#define Tc      2		//Tempo chegada do processo
#define Pr      3		//Prioridade do Processo
#define Tq      3       //Tempo de quantum



int get_matrix (int (*m)[COLS], FILE *fp)
{
	fscanf(fp, "%*[^\n]");
	for (int row = 0; row < ROWS; row++) 
	{
		for (int col = 0; col < COLS; col++) 
		{
			if (fscanf (fp, "%d", &m[row][col]) != 1) 
			{
				fprintf (stderr, "error reading m[%d][%d]\n", row, col);
			return 0;
			}
		}
	}
  
	return 1;
}

void print_matrix (int (*m)[COLS])
{
	for (int row = 0; row < ROWS; row++)
	{
		for (int col = 0; col < COLS; col++)
		{
			printf (col ? " %4d" : "%4d", m[row][col]);

		}

		putchar ('\n');

	}
}


void round_robin_runner (int (*matrix)[COLS])
{
    int aux[ROWS];
	int aux_td[ROWS];
	int waiting_time = 0;
	int life_time = 0;
	int timer;
    int rows = ROWS;
    int counter;
	float average_waiting_time;
	float average_life_time;

    
    
    for (int index = 0; index < ROWS; index ++) {
        aux[index] = matrix[index][Td];
    }
  
  
    //printf("\n\nPi\tTEMPO DE EXECUÇÃO\t CICLO DE VIDA\t TEMPO DE ESPERA\n");
    for(int index = 0, timer = 0; rows != 0;)
	{
		if (aux[index] <= Tq && aux[index > 0])
		{
			timer = timer + aux[index];
			aux[index] = 0;
			counter = 1;
		}

		else if(aux[index] > 0)
		{
			aux[index] = aux[index] - Tq;
			timer = timer + Tq;
		} 

		if (aux[index] == 0 && counter == 1)
		{
			rows--;
			//printf("\n%d\t\t%d\t\t\t%d\t\t%d", index + 1, matrix[index][Td], timer - matrix[index][Tc], timer - matrix[index][Tc] - matrix[index][Td]);
			waiting_time = waiting_time + timer - matrix[index][Tc] - matrix[index][Td];
			life_time = life_time + timer - matrix[index][Tc];
		}

		if (index == (ROWS-1))
		{
			index = 0;
		}
		else if (matrix[index + 1][Tc] <= timer)
		{
			index ++;
		}
		else
		{
			index = 0;
		}
	}

		average_life_time = (float) life_time/ROWS;
		average_waiting_time = (float) waiting_time / ROWS;

	printf("\n\nTempo de espera medio : %f\nTempo de vida medio : %f\n\n", average_waiting_time, average_life_time);
	printf("\nThread RR Finalizada");


}

void sort_arrival_time (int (*matrix)[COLS])
{
    

	for (int index = 0; index < ROWS; index++)
	{
		for (int row = index+1; row < ROWS; row++)
		{   
			if (matrix[row][Tc] < matrix[index][Tc])
			{
				int swap[4];
				for(int aux = 0; aux < 4; aux++ )
				{
					swap[aux]      		= matrix[index][aux];
					matrix[index][aux]  = matrix[row][aux];
					matrix[row][aux]	= swap[aux];
				}
			}
		
		}
	}

	print_matrix(matrix);
	round_robin_runner(matrix);

}

int main (int argc, char **argv) {
  
	int matrix[ROWS][COLS] = {{0}};   
	/* use filename provided as 1st argument (stdin by default) */
	FILE *fp = argc > 1 ? fopen (argv[1], "r") : stdin;
	
	if (fp == NULL) {  /* validate file open for reading in caller */
	perror ("file open failed");
	return 1;
  	}
  
	if (!get_matrix(matrix, fp))    
	{
		return 1;
	}
	sort_arrival_time (matrix);
	fclose (fp);
}