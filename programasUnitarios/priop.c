#include <stdio.h>

#define ROWS 10		//numero linhas
#define COLS 4		//numero colunas
#define Id   0		//Identificador do processo
#define Td   1		//Tempo duração do processo
#define Tc   2		//Tempo chegada do processo
#define Pr   3		//Prioridade do Processo



int get_matrix (int (*m)[COLS], FILE *fp)
{
	fscanf(fp, "%*[^\n]");
	for (int row = 0; row < ROWS; row++) 
	{
		for (int col = 0; col < COLS; col++) 
		{
			if (fscanf (fp, "%d", &m[row][col]) != 1) 
			{
				fprintf (stderr, "error reading m[%d][%d]\n", row, col);
			return 0;
			}
		}
	}
  
	return 1;
}

void print_matrix (int (*m)[COLS])
{
	for (int row = 0; row < ROWS; row++)
	{
		for (int col = 0; col < COLS; col++)
		{
			printf (col ? " %4d" : "%4d", m[row][col]);

		}

		putchar ('\n');

	}
}

void priop_runner(int (*matrix)[COLS])
{
    int debugger = 0;
    int aux[ROWS];
	int waiting_time[ROWS+1];
	int life_time[ROWS+1];
	int total_waiting_time = 0;
	int total_life_time = 0;
    int priority = 0 ;
    int lowest_priority_process;
    int count;
    int timer;
    int low;
	float average_waiting_time = 0;
	float average_life_time = 0;
	waiting_time[0] = 0;

    for (int index = 0; index < ROWS; index++)
    {
    aux[index] = matrix[index][Td];
            // if (priority < matrix[index][Pr]);
            // {
            //     priority = matrix[index][Pr];
            //     lowest_priority_process = index;
            // }
    }
    matrix[9][Pr] = 10000;

    for (timer = 0; count != ROWS; timer ++)
    {
        // low = lowest_priority_process;
        low = 9;
        for(int index = 0; index < ROWS; index++)
        {

            if((matrix[low][Pr] > matrix[index][Pr]) && (matrix[index][Tc] <= timer) && (matrix[index][Td] > 0))
            {
                    low = index; 

            }

        }

        matrix[low][Td] = matrix[low][Td] - 1;




        if( matrix[low][Td] == 0 )
        {   
			count ++;
            waiting_time[low] = timer + 1 - matrix[low][Tc] - aux[low];
            life_time[low] = timer + 1 - matrix[low][Tc];

            total_life_time += life_time[low];
            total_waiting_time += waiting_time[low];
        }

    }


    average_life_time = (float)total_life_time/ROWS;
    average_waiting_time = (float)total_waiting_time/ROWS;

    printf("\n\n\nTEMPO DE ESPERA MÉDIO = %.2f \n", average_waiting_time);
    printf("CICLO DE VIDA MÉDIO = %.2f \n", average_life_time);


}


void sort_arrival_time (int (*matrix)[COLS])
{
    

	for (int index = 0; index < ROWS; index++)
	{
		for (int row = index+1; row < ROWS; row++)
		{   
			if (matrix[row][Tc] < matrix[index][Tc])
			{
				int swap[4];
				for(int aux = 0; aux < 4; aux++ )
				{
					swap[aux]      		= matrix[index][aux];
					matrix[index][aux]  = matrix[row][aux];
					matrix[row][aux]	= swap[aux];
				}
			}
		
		}
	}

	// print_matrix(matrix);
	priop_runner(matrix);

}



int main (int argc, char **argv) {
  
	int matrix[ROWS][COLS] = {{0}};   
	/* use filename provided as 1st argument (stdin by default) */
	FILE *fp = argc > 1 ? fopen (argv[1], "r") : stdin;
	
	if (fp == NULL) {  /* validate file open for reading in caller */
	perror ("file open failed");
	return 1;
  }
  
	if (!get_matrix(matrix, fp))    
	{
		return 1;
	}
	fclose (fp);
	sort_arrival_time(matrix);

}
