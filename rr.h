
#define ROWS    10		//numero linhas
#define COLS    4		//numero colunas
#define Id      0		//Identificador do processo
#define Td      1		//Tempo duração do processo
#define Tc      2		//Tempo chegada do processo
#define Pr      3		//Prioridade do Processo
#define Tq      3       //Tempo de quantum

void *round_robin_runner (void *id)
{

    int aux[ROWS];
	int waiting_time = 0;
	int life_time = 0;
    int rows = ROWS;
    int counter;
	float average_waiting_time;
	float average_life_time;

    printf("\n Thread RR Inicializada");

    int matrix[ROWS][COLS] = {{0}};   
	FILE *fp = fopen("entrada.txt", "r");
	
	fscanf(fp, "%*[^\n]");  // Read and discard a line
	for (int index = 0; index < ROWS; index++)
		fscanf(fp, "%d\t%d\t%d\t%d", &matrix[index][Id], &matrix[index][Td], &matrix[index][Tc], &matrix[index][Pr]);
	fclose (fp);

	for (int index = 0; index < ROWS; index++)
	{
		for (int row = index+1; row < ROWS; row++)
		{   
			if (matrix[row][Tc] < matrix[index][Tc])
			{
				int swap[4];
				for(int aux = 0; aux < 4; aux++ )
				{
					swap[aux]      		= matrix[index][aux];
					matrix[index][aux]  = matrix[row][aux];
					matrix[row][aux]	= swap[aux];
				}
			}
		
		}
	}




    
    
    for (int index = 0; index < ROWS; index ++) {
        aux[index] = matrix[index][Td];
    }
  
  
    printf("\n\nPi\tTempo de Execução\t Ciclo de Vida\t Tempo de Espera  RR\n");
    for(int index = 0, timer = 0; rows != 0;)
	{
		if (aux[index] <= Tq && aux[index > 0])
		{
			timer = timer + aux[index];
			aux[index] = 0;
			counter = 1;
		}

		else if(aux[index] > 0)
		{
			aux[index] = aux[index] - Tq;
			timer = timer + Tq;
		} 

		if (aux[index] == 0 && counter == 1)
		{
			rows--;
			printf("\n%d\t\t%d\t\t\t%d\t\t%d", index + 1, matrix[index][Td], timer - matrix[index][Tc], timer - matrix[index][Tc] - matrix[index][Td]);
			waiting_time = waiting_time + timer - matrix[index][Tc] - matrix[index][Td];
			life_time = life_time + timer - matrix[index][Tc];
		}

		if (index == (ROWS-1))
		{
			index = 0;
		}
		else if (matrix[index + 1][Tc] <= timer)
		{
			index ++;
		}
		else
		{
			index = 0;
		}
	}

		average_life_time = (float) life_time/ROWS;
		average_waiting_time = (float) waiting_time / ROWS;

    printf("\n\n\nTEMPO DE ESPERA MÉDIO RR = %.2f \n", average_waiting_time);
    printf("CICLO DE VIDA MÉDIO RR = %.2f \n", average_life_time);
	printf("\nThread RR Finalizada\n");

	pthread_exit (NULL) ;


}