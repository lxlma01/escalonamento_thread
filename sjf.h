

#define ROWS    10		//numero linhas
#define COLS    4		//numero colunas
#define Id      0		//Identificador do processo
#define Td      1		//Tempo duração do processo
#define Tc      2		//Tempo chegada do processo
#define Pr      3		//Prioridade do Processo
#define Tq      3       //Tempo de quantum



void *sjf_runner(void *id)
{

    printf("\n Thread SJF Inicializada");

    int matrix[ROWS][COLS] = {{0}};   
	FILE *fp = fopen("entrada.txt", "r");
	
	fscanf(fp, "%*[^\n]");  // Read and discard a line
	for (int index = 0; index < ROWS; index++)
		fscanf(fp, "%d\t%d\t%d\t%d", &matrix[index][Id], &matrix[index][Td], &matrix[index][Tc], &matrix[index][Pr]);
	fclose (fp);

	for (int index = 0; index < ROWS; index++)
	{
		for (int row = index+1; row < ROWS; row++)
		{   
			if (matrix[row][Tc] < matrix[index][Tc])
			{
				int swap[4];
				for(int aux = 0; aux < 4; aux++ )
				{
					swap[aux]      		= matrix[index][aux];
					matrix[index][aux]  = matrix[row][aux];
					matrix[row][aux]	= swap[aux];
				}
			}
		
		}
	}

    int aux[ROWS];
	int waiting_time[ROWS+1];
	int life_time[ROWS+1];
	int total_waiting_time = 0;
	int total_life_time = 0;
	float average_waiting_time;
	float average_life_time;
	waiting_time[0] = 0; 	

    aux[0] = matrix[0][Td] + matrix[0][Tc];

    if (aux[0] > matrix[ROWS-1][Tc]) 
    {
        for (int index = 0; index < ROWS; index++)
	    {
		    for (int row = index+1; row < ROWS; row++)
		    {   
			    if (matrix[row][Tc] < matrix[index][Tc])
			    {
			    	int swap[4];
			    	for(int aux = 0; aux < 4; aux++ )
			    	{
			    		swap[aux]      		= matrix[index][aux];
			    		matrix[index][aux]  = matrix[row][aux];
			    		matrix[row][aux]	= swap[aux];
			    	}
			    }
		
		    }
	    }
    
        life_time[0] = matrix[0][Td];
        waiting_time[0] = 0;

        for(int index = 1; index < ROWS; index++)
            aux[index] = abs(aux[index - 1] + matrix[index][Td]);

    }
    else

    {

        for (int index = 1; index < ROWS; index++)
        {
            for (int row = index+1; row < ROWS; row++)
            {   
                if (matrix[row][Tc] < matrix[index][Tc])
                {
                    int swap[4];
                    for(int aux = 0; aux < 4; aux++ )
                    {
                        swap[aux]      		= matrix[index][aux];
                        matrix[index][aux]  = matrix[row][aux];
                        matrix[row][aux]	= swap[aux];
                    }
                }
            }
        }
        aux[1] = matrix[1][Td] + matrix[1][Tc];

        for (int index = 2; index < ROWS; index++)
        {
            for (int row = index+1; row < ROWS; row++)
            {   
                if (matrix[row][Tc] < matrix[index][Tc])
                {
                    int swap[4];
                    for(int aux = 0; aux < 4; aux++ )
                    {
                        swap[aux]      		= matrix[index][aux];
                        matrix[index][aux]  = matrix[row][aux];
                        matrix[row][aux]	= swap[aux];
                    }
                }
            }
        }

        life_time[0] = matrix[0][Td];
        waiting_time[0] = 0;

        for(int index = 2; index < ROWS; index++)
            aux[index] = abs(aux[index - 1] + matrix[index][Td]);

    }



    for(int index = 0; index < ROWS; index++)
    {
        life_time[index] = abs(aux[index] - matrix[index][Tc]);
        total_life_time += life_time[index];
    }
    average_life_time = (float)total_life_time/ROWS;

    for(int index = 0; index < ROWS; index++)
    {
        waiting_time[index] = abs(aux[index] - matrix[index][Td]);
        total_waiting_time += waiting_time[index];
    }
    average_waiting_time = (float)total_waiting_time/ROWS;

	printf("\n\nPi\tTempo de Execução\t Ciclo de Vida\n");

	for(int index = 0; index < ROWS; index++)
	{
		printf("%d\t\t%d\t\t\t%d\n", index + 1, waiting_time[index], life_time[index]);
	}

    printf("\n\n\nTEMPO DE ESPERA MÉDIO SJF = %.2f \n", average_waiting_time);
    printf("CICLO DE VIDA MÉDIO SJF = %.2f \n", average_life_time);
    printf("\n Thread SJF Finalizada\n");

    pthread_exit (NULL) ;	
}