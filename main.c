#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include "fcfs.h"
#include "prioc.h"
#include "priop.h"
#include "rr.h"
#include "sjf.h"
#include "srtf.h"

#define num_threads 6


int main ()
{
    pthread_t threads_pointer[num_threads];

    pthread_create(&threads_pointer[0], NULL, fcfs_runner, (void *)0);
    pthread_create(&threads_pointer[1], NULL, round_robin_runner, (void*)1);
    pthread_create(&threads_pointer[2], NULL, sjf_runner, (void*)2);
    pthread_create(&threads_pointer[3], NULL, srtf_runner, (void*)3);
    pthread_create(&threads_pointer[4], NULL, prioc_runner, (void*)4);
    pthread_create(&threads_pointer[5], NULL, priop_runner, (void*)5);

    
    for(int index = 0; index < num_threads; index++)
    {
        pthread_join(threads_pointer[index], NULL);
    }
        
}




