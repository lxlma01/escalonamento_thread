

#define ROWS    10		//numero linhas
#define COLS    4		//numero colunas
#define Id      0		//Identificador do processo
#define Td      1		//Tempo duração do processo
#define Tc      2		//Tempo chegada do processo
#define Pr      3		//Prioridade do Processo
#define Tq      3       //Tempo de quantum


void *fcfs_runner(void *id)
{


	int matrix[ROWS][COLS] = {{0}};   
	FILE *fp = fopen("entrada.txt", "r");
	fscanf(fp, "%*[^\n]");
	for (int index = 0; index < ROWS; index++)
	{
		fscanf(fp, "%d\t%d\t%d\t%d", &matrix[index][Id], &matrix[index][Td], &matrix[index][Tc], &matrix[index][Pr]);
	}

	printf("\n Thread FCFS Inicializada");

	for (int index = 0; index < ROWS; index++)
	{
		for (int row = index+1; row < ROWS; row++)
		{   
			if (matrix[row][Tc] < matrix[index][Tc])
			{
				int swap[4];
				for(int aux = 0; aux < 4; aux++ )
				{
					swap[aux]      		= matrix[index][aux];
					matrix[index][aux]  = matrix[row][aux];
					matrix[row][aux]	= swap[aux];
				}
			}
		
		}
	}

	int aux[ROWS];
	int waiting_time[ROWS+1];
	int life_time[ROWS+1];
	int total_waiting_time = 0;
	int total_life_time = 0;
	float average_waiting_time;
	float average_life_time;
	waiting_time[0] = 0; 	

	printf("\n\n\nPi\tTEMPO DE EXECUÇÃO  FCFS");
	
	if(matrix[0][Tc] != 0) 
		printf("[x]\t1");

	aux[0] = matrix[0][Tc];
	printf("\n%d\t\t%d", matrix[0][Id], matrix[0][Tc]);

	for(int index = 1; index < ROWS; index++) {
		if(matrix[index][Tc] > (aux[index-1] + matrix[index - 1][Td])) {
			printf("\n%d \t\t%d", matrix[index][Id], matrix[index][Tc]);
			aux[index] = matrix[index][Tc];
		}
		else {
			aux[index] = aux[index-1] + matrix[index-1][Td];
			printf("\n%d \t\t%d" , matrix[index][Id], aux[index]);
		}
	}


	for (int index = 0; index < ROWS; index++) {
		waiting_time[index + 1] = aux[index] - matrix[index][Tc];
		total_waiting_time += waiting_time[index+1];
		life_time[index] = aux[index] - matrix[index][Tc] + matrix[index][Td];
		total_life_time += life_time[index];
	}

	printf("\n\n\nPi\tTEMPO DE ESPERA  FCFS\t");

	for (int index = 0; index < ROWS; index++) {
		printf("\n%d\t\t%d", matrix[index][Id], waiting_time[index+1]);
	}

	printf("\n\n\n CICLO DE VIDA FCFS\t");

	for (int index = 0; index < ROWS; index++) {
		printf("\n%d\t\t%d", matrix[index][Id], life_time[index]);
	}
	
	average_waiting_time = (float)total_waiting_time/ROWS;
	average_life_time = (float)total_life_time/ROWS;

	printf("\n\n\nTEMPO DE ESPERA MÉDIO FCFS = %.2f \n", average_waiting_time);
	printf("CICLO DE VIDA MÉDIO FCFS = %.2f \n", average_life_time);
    printf("\n Thread FCFS Finalizada \n");
    
	fclose (fp);
    pthread_exit (NULL) ;	
}