

#define ROWS    10		//numero linhas
#define COLS    4		//numero colunas
#define Id      0		//Identificador do processo
#define Td      1		//Tempo duração do processo
#define Tc      2		//Tempo chegada do processo
#define Pr      3		//Prioridade do Processo
#define Tq      3       //Tempo de quantum



void *srtf_runner(void *id)
{
    printf("\n Thread SRTF Inicializada");

 	int matrix[ROWS][COLS] = {{0}};   
	FILE *fp = fopen("entrada.txt", "r");
	
	fscanf(fp, "%*[^\n]");  // Read and discard a line
	for (int index = 0; index < ROWS; index++)
		fscanf(fp, "%d\t%d\t%d\t%d", &matrix[index][Id], &matrix[index][Td], &matrix[index][Tc], &matrix[index][Pr]);
	fclose (fp);

	for (int index = 0; index < ROWS; index++)
	{
		for (int row = index+1; row < ROWS; row++)
		{   
			if (matrix[row][Tc] < matrix[index][Tc])
			{
				int swap[4];
				for(int aux = 0; aux < 4; aux++ )
				{
					swap[aux]      		= matrix[index][aux];
					matrix[index][aux]  = matrix[row][aux];
					matrix[row][aux]	= swap[aux];
				}
			}
		
		}
	}


    int aux[ROWS + 1];
	int com[ROWS + 1];
	int waiting_time[ROWS+1];
	int life_time[ROWS+1];
	int total_waiting_time = 0;
	int total_life_time = 0;
	int timer1;
	int timer2 = 1;
	float average_waiting_time = 0;
	float average_life_time = 0;
	waiting_time[0] = 0; 	

	aux[0] = matrix[0][Td] - 1;
	timer1 = aux[0];

	com[0] = matrix[0][Tc] + 1;

	for(int index = 1; index < ROWS; index++)
	{
		aux[index] = matrix[index][Td];
	}

	if(aux[0] > aux[1])
	{
		aux[ROWS-1] = timer2;

		for(int index = 0; index < ROWS; index++)
		{
			for(int row = index + 1; row < ROWS; row++)
			{
				if(aux[index] > aux[row])
				{
					int swap[4];
					for(int aux = 0; aux < 4; aux++ )
					{
					swap[aux]      		= matrix[index][aux];
					matrix[index][aux]  = matrix[row][aux];
					matrix[row][aux]	= swap[aux];
					}
				}
			}
		}

		matrix[0][Id] = aux[0];
		matrix[0][Tc] = aux[0];

		if (matrix[1][Tc] < matrix[2][Tc])
		{
			com[1] = com[0] + aux[1];

			for(int index = 0; index < ROWS; index++)
			{
				for(int row = index + 1; row < ROWS; row++)
				{
					if(matrix[index][Tc] > matrix[row][Tc])
					{
						int swap[4];
						for(int aux = 0; aux < 4; aux++ )
						{
						swap[aux]      		= matrix[index][aux];
						matrix[index][aux]  = matrix[row][aux];
						matrix[row][aux]	= swap[aux];
						}
					}
				}
			}
		
			com[2] = com[1] + aux[1];

			for(int index = 0; index < ROWS; index++)
			{
				for(int row = index + 1; row < ROWS; row++)
				{
					if(aux[index] > aux[row])
					{
						int swap[4];
						for(int aux = 0; aux < 4; aux++ )
						{
						swap[aux]      		= matrix[index][aux];
						matrix[index][aux]  = matrix[row][aux];
						matrix[row][aux]	= swap[aux];
						}
					}
				}
			}
			life_time[0] = 0;
			life_time[1] = aux[0];

			waiting_time[0] = 0;

			for(int index = 3; index < ROWS; index++)
			{
				com[index] = abs(com[index-1] + aux[index]);
			}
			for(int index = 1; index < ROWS; index++)
			{
				life_time[index] = abs(com[index] - matrix[index][Tc]);
			}

			for(int index = 1; index < ROWS; index++)
			{
				waiting_time[index] = abs(life_time[index] - aux[index]);
				if (waiting_time[index] == timer1)
					waiting_time[index] = waiting_time[index] - 1;
			}
		}

		else
		{
			for(int index = 0; index < ROWS; index++)
			{
				for(int row = index + 1; row < ROWS; row++)
				{
					if(matrix[index][Tc] > matrix[row][Tc])
					{
						int swap[4];
						for(int aux = 0; aux < 4; aux++ )
						{
						swap[aux]      		= matrix[index][aux];
						matrix[index][aux]  = matrix[row][aux];
						matrix[row][aux]	= swap[aux];
						}
					}
				}
			}
			com[1] = com[0] + aux[2];

			for(int index = 0; index < ROWS; index++)
			{
				for(int row = index + 1; row < ROWS; row++)
				{
					if(matrix[index][Td] > matrix[row][Td])
					{
						int swap[4];
						for(int aux = 0; aux < 4; aux++ )
						{
						swap[aux]      		= matrix[index][aux];
						matrix[index][aux]  = matrix[row][aux];
						matrix[row][aux]	= swap[aux];
						}
					}
				}
			}

			com[2] = com[1] + aux[1];

			if(matrix[1][Tc]>matrix[2][Tc])
			{
				int swap[4];
				for(int aux = 0; aux < 4; aux++ )
				{
					swap[aux] 		= matrix[1][aux];
					matrix[1][aux]  = matrix[2][aux];
					matrix[2][aux]	= swap[aux];
				}
			}
			life_time[0] = 0;
			life_time[1] = aux[0];

			waiting_time[0] = 0;

			for(int index = 3; index < ROWS; index++)
			{
				com[index] = abs(com[index-1] + aux[index]);
			}
			for(int index = 1; index < ROWS; index++)
			{
				life_time[index] = abs(com[index] - matrix[index][Tc]);
			}

			for(int index = 1; index < ROWS; index++)
			{
				if(timer1 == aux[index])
					aux[index] += 1;
				waiting_time[index] = abs(life_time[index] - aux[index]);
				if (waiting_time[index] == timer1)
					waiting_time[index] = waiting_time[index] - 1;
			}
		
			
		}

		for (int index = 0; index< ROWS; index++)
			total_life_time += life_time[index];

		for (int index = 0; index< ROWS; index++)
			total_waiting_time += waiting_time[index];

		average_waiting_time = (float)total_waiting_time/ROWS;
		average_life_time = (float)total_life_time/ROWS;

		printf("\n\nPi\tTempo de Execução\t Ciclo de Vida\t Tempo de Espera  SRTF\n");

		for(int index = 0; index < ROWS; index++)
		{
			printf("%d\t\t%d\t\t%d\n", index + 1, waiting_time[index], life_time[index]);
		}

    	printf("\n\n\nTEMPO DE ESPERA MÉDIO SRTF= %.2f \n", average_waiting_time);
    	printf("CICLO DE VIDA MÉDIO SRTF= %.2f \n", average_life_time);
        printf("\nThread SRTF Finalizada");

		pthread_exit (NULL) ;	
		
	}

	else
	{
		aux[0] = aux[0] + timer2;
		com[0] = aux[0];
		life_time[0] = aux[0];
		waiting_time[0] = 0;

		for(int index = 0; index < ROWS; index++)
		{
			for(int row = index + 1; row < ROWS; row++)
			{
				if(aux[index] > aux[row])
				{
					int swap[4];
					for(int aux = 0; aux < 4; aux++ )
					{
					swap[aux]      		= matrix[index][aux];
					matrix[index][aux]  = matrix[row][aux];
					matrix[row][aux]	= swap[aux];
					}
				}
			}
		}

		for(int index = 1; index < ROWS; index++)
		{
			com[index] = abs(com[index-1] + aux[index]);
		}
		for(int index = 0; index < ROWS; index++)
		{
			life_time[index] = abs(com[index] - matrix[index][Tc]);
		}
		for(int index = 1; index < ROWS; index++)
		{
			waiting_time[index] = abs(life_time[index] - aux[index]);
		}
	
		for (int index = 0; index< ROWS; index++)
			total_life_time += life_time[index];

		for (int index = 0; index< ROWS; index++)
			total_waiting_time += waiting_time[index];

		average_waiting_time = (float)total_waiting_time/ROWS;
		average_life_time = (float)total_life_time/ROWS;

		printf("\n\nPi\tTempo de Execução\t Ciclo de Vida SRTF\n");

		for(int index = 0; index < ROWS; index++)
		{
			printf("%d\t\t%d\t\t\t%d\n", index + 1, waiting_time[index], life_time[index]);
		}

    	printf("\n\n\nTEMPO DE ESPERA MÉDIO SRTF= %.2f \n", average_waiting_time);
    	printf("CICLO DE VIDA MÉDIO SRTF= %.2f \n", average_life_time);
		printf("\nThread SRTF Finalizada\n");

		pthread_exit (NULL) ;	
	}
	


    
}